Turbine
=======

Turbine is an easy to set-up and manage load-balancer and reverse proxy management interface complete with RESTful API to enable automated third-party application integration.

